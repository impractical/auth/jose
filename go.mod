module impractical.co/auth/jose

require (
	github.com/pkg/errors v0.8.0
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793 // indirect
	gopkg.in/square/go-jose.v2 v2.1.8
)
